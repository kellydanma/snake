# SnakeGame

🐍 A simple game based on the 1976 arcade game, Blockade. A (somewhat) hilarious attempt to de-noob myself and learn about JavaScript: objects, data structures, etc.

## Installing SnakeGame
First, copy this repo onto your device:

```
git clone https://github.com/kellydanma/SnakeGame.git
```

Navigate to SnakeGame directory, then, run the game from the command line using:

```
open index.html
```

Have fun!
