class Snake {

     constructor() {
          this.pos = createVector(0, 0); // starting position of the snake
          this.xSpeed = SCL;
          this.ySpeed = 0;
          this.total = 0; // how much food snake has eaten
          this.tail = []; // store vector positions for each part of tail
     }

     moveDir(xDir, yDir) { // keeps track of snake movement
          this.xSpeed = xDir * SCL;
          this.ySpeed = yDir * SCL;
     }

     eatsFood(foodPos) {
          var d = dist(this.pos.x, this.pos.y, foodPos.x, foodPos.y);
          if (d < 1) this.total++; // food snake has eaten increases
          return d < 1 ? true : false;
     }

     losesTail() {
          for (var i = 0; i < this.tail.length; i++) {
               const d = this.pos.dist(this.tail[i]);
               if (d < 1) {
                    this.total = 0;
                    this.tail = [];
                    return true;
               }
          }
          return false; // otherwise, lose tail
     }

     update() {
          if (this.total === this.tail.length) {
               for (var i = 0; i < this.tail.length - 1; i++) {
                    this.tail[i] = this.tail[i+1];
               }
          }
          if (this.total > 0) { // if snake has eaten food
               this.tail[this.total - 1] = this.pos.copy();
          }
          const tempX = this.pos.x + this.xSpeed;
          const tempY = this.pos.y + this.ySpeed;
          this.pos.x = constrain(tempX, 0, width - SCL);
          this.pos.y = constrain(tempY, 0, height - SCL);
          this.show();
     }

     show() {
          fill(255); // rectangular snake
          rect(this.pos.x, this.pos.y, SCL, SCL);
          for (var i = 0; i < this.tail.length; i++) {
               rect(this.tail[i].x, this.tail[i].y, SCL, SCL);
          }
     }

}