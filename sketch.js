let snake, food, rows, cols;
let lostTail = false;
const SCL = 20;

function setup() {
     createCanvas(600,600);
     cols = floor(width/SCL);
     rows = floor(height/SCL);
     pickFoodLocation();
     snake = new Snake();
     frameRate(10); // speed of snake
}

function draw() {
     background(51);
     if (snake.eatsFood(food)) pickFoodLocation();
     fill(255, 0, 0);
     rect(food.x, food.y, SCL, SCL);
     snake.losesTail();
     snake.update();
     snake.show();
     textSize(32);
     text( snake.total, 10, 30 );
     if (snake.losesTail()) lostTail = true;
     if (snake.total === 0 && lostTail) text("Game Over", 10, 60);
}

function keyPressed() { // user input
     if (keyCode === UP_ARROW) {
          snake.moveDir(0,-1);
     }
     else if (keyCode === DOWN_ARROW) {
          snake.moveDir(0,1);
     }
     else if (keyCode === LEFT_ARROW) {
          snake.moveDir(-1,0);

     }
     else if (keyCode === RIGHT_ARROW) {
          snake.moveDir(1,0);
     }
}

function pickFoodLocation() { // generate a random food location
     food = createVector(floor(random(cols)), floor(random(rows)));
     food.mult(SCL);
}